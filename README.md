Open Finder: Click on the Finder icon located in the dock at the bottom of your screen. The Finder is represented by a blue and white smiley face.

Go to Applications: In the Finder window, locate the "Applications" folder. You can usually find it in the left-hand sidebar.

Open Utilities: Inside the Applications folder, locate and open the "Utilities" folder.
Launch Terminal: In the Utilities folder, you should see an application named "Terminal". Double-click on it to launch the Terminal application.

Check if Python is installed: Open a terminal or command prompt on your computer and type python --version or python3 --version. If Python is already installed, it will display the version number. If not, you'll need to install Python.
Download Python: If you don't have Python installed, you can download it for free from the official Python website: https://www.python.org/downloads/. Make sure to download the appropriate version for your operating system (Windows, macOS, or Linux).

Install Python: Once the Python installer is downloaded, run the installer program and follow the instructions. During the installation, you may be asked to choose optional features and specify the installation location. It's generally recommended to select the default options.

Verify the Installation: After the installation is complete, open a new terminal or command prompt and again type python --version or python3 --version. It should now display the Python version number, confirming that Python is successfully installed.

Install Visual Studio code: https://code.visualstudio.com/

Use the command line to `cd` into the directory you want this code stored in, to go back use `cd ..` Type `ls` to see the contents of your folders for navigation. 

In GitLab go to the blue Clone button on the ride side and clone with HTTPS. This will copy a  link, when you're in the right directory in your terminal type `git clone ` and then paste that link. you should now be in the paragraph_break repo. 

Once you can see `paragraph_break` in your command line run the command `python paragraph_break.py`.
Text should be outputted and you can copy the text.

To change the input text type `code .` after `paragraph_break` to open Visual Studio Code. You can paste the text you want broken, make sure it's in quotes. You will need to delete the previous text and paste the next text in on line 1 of the document, make sure to leave your variable `text = `

You may need to do this in sections as the text is very long and it could cause the program to slow down or fail if you try to do much text at once.
